-- v1.0.2

local alarm = grp.getvalue("3/1/1")
local lux_sensor = grp.getvalue("4/2/1")
local lux_sensor_metadata = grp.find("4/2/1")
local contact_hall_lumi =  grp.tag("CONTACT_HALL_LUMI")
local contact_hall_lumi_ni =  grp.tag("CONTACT_HALL_LUMI-NI")
local night = grp.getvalue("6/1/35")

local high_threshold = 1500 --lux
local low_threshold = 1000 -- lux
local frame_tolerance = 3600 -- 1h

if storage.get("last_dataframe_time") == nil then
    storage.set("last_dataframe_time", os.time())
end

function valid_frame(lux_sensor_metadata)
    local last_dataframe_time = storage.get("last_dataframe_time")
    storage.set("current_frame_time", os.time())
    local current_frame_time = storage.get("current_frame_time")
    local delta_time = current_frame_time - last_dataframe_time
    storage.set("last_dataframe_time", lux_sensor_metadata["updatetime"])

    if delta_time >= frame_tolerance or lux_sensor > 10000 then
        return false
    elseif delta_time < frame_tolerance and lux_sensor < 10000 then
        return true
    else
        return log("Error on delta_time status")
    end
end

function light_on()
    for i,obj in ipairs(contact_hall_lumi) do
        obj:write(0)
        sleep(1)
    end
    for i,obj in ipairs(contact_hall_lumi_ni) do
        obj:write(1)
        sleep(1)
    end
end

function light_off()
    for i,obj in ipairs(contact_hall_lumi) do
        obj:write(1)
        sleep(1)
    end
    for i,obj in ipairs(contact_hall_lumi_ni) do
        obj:write(0)
        sleep(1)
    end
end

function debug_mode()
    log("Some not handled situations occured here's the situation:")
    log("alarm = " .. alarm)
    log("night = " .. night)
    log("valid_frame(lux_sensor_metadata) = " .. valid_frame(lux_sensor_metadata))
    log("lux_sensor = " .. lux_sensor)
    log("low_threshold = " .. low_threshold)
    log("high_threshold = " .. high_threshold)
end

function main()
    if not alarm and night then
        light_on()
    elseif alarm then
        light_off()
    elseif not alarm and valid_frame(lux_sensor_metadata) then
        if lux_sensor < low_threshold then
            light_on()
        elseif lux_sensor > high_threshold then
            light_off()
        end
    elseif not alarm and not valid_frame(lux_sensor_metadata) then
        if night then
            light_on()
        elseif not night then
            light_off()
        else
            log("Error on night status")
        end
    else
        log("Unexpected conditions!!!")
        debug_mode()
    end
end

main()

